package akka.banking;

import akka.actor.ActorRef;

import java.io.Serializable;

/**
 * Created by babiya on 11.11.2015.
 */
public class ChangeAmountMessage {
    private String accountId;
    private Integer amount;


    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public ChangeAmountMessage(String accountId, Integer amount) {
        this.accountId = accountId;
        this.amount = amount;
    }
}
