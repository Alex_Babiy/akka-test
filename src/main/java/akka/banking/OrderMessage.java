package akka.banking;

/**
 * Created by babiya on 11.11.2015.
 */
public class OrderMessage {
    private String accountId;
    private Integer balance;

    public OrderMessage(String accountId, Integer balance) {
        this.accountId = accountId;
        this.balance = balance;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }
}
