package akka.banking;

/**
 * Created by babiya on 11.11.2015.
 */
import akka.actor.Actor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.actor.UntypedActorFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class TransactionProcessorActor extends UntypedActor {

    private static final String RESULT_FILE = "result.csv";
    private static final String PROCESSOR_PATH = "akka://bankSystem/user/processor";

    private static HashMap<String, ActorRef> accountProcessors = new HashMap<String, ActorRef>();

    @Override
    public void onReceive(Object message) throws IOException {
        if (message instanceof ChangeAmountMessage){
            processChangeAmountMessage((ChangeAmountMessage) message);
        } else if (message instanceof FinalizeResultMessage){
            processFinalizeResultMessage((FinalizeResultMessage)message);
        } else if (message instanceof OrderMessage){
            processOrderMessage((OrderMessage) message);
        } else {
            unhandled(message);
        }
    }

    private void processFinalizeResultMessage(FinalizeResultMessage message){
        for(Map.Entry<String, ActorRef> entry : accountProcessors.entrySet()){
            entry.getValue().tell(message, this.getSelf());
        }
    }

    private void processChangeAmountMessage(final ChangeAmountMessage changeAmountMessage){
        ActorRef accountProcessor = accountProcessors.get(changeAmountMessage.getAccountId());
        if ( accountProcessor == null ) {
            accountProcessor = getContext().actorOf(new Props(new UntypedActorFactory() {
                @Override
                public Actor create() {
                    return new AccountActor(changeAmountMessage.getAccountId(),
                            context().actorFor(PROCESSOR_PATH),
                            0);
                }
            }));
            accountProcessors.put(changeAmountMessage.getAccountId(),accountProcessor);
        }
        accountProcessor.tell(changeAmountMessage);
    }

    private void processOrderMessage(OrderMessage orderMessage) throws IOException {
        FileWriter writer = new FileWriter(RESULT_FILE,true);
        writer.write(orderMessage.getAccountId() + ";" + orderMessage.getBalance() + "\n");
        writer.close();
    }
}
