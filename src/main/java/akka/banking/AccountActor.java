package akka.banking;

/**
 * Created by babiya on 11.11.2015.
 */
import akka.actor.ActorRef;
import akka.actor.UntypedActor;


public class AccountActor extends UntypedActor {

    private String owner;

    private int balance;

    private ActorRef transactionProcessor;

    public AccountActor(String name, ActorRef transactionProcessor, int bal) {
        this.owner = name;
        this.transactionProcessor = transactionProcessor;
        this.balance = bal;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if(message instanceof ChangeAmountMessage){
            balance += ((ChangeAmountMessage) message).getAmount();
        } else if (message instanceof FinalizeResultMessage){
            getSender().tell(new OrderMessage(owner,balance));
        } else {
            unhandled(message);
        }
    }
}