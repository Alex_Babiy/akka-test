package akka.banking;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

import java.io.BufferedReader;
import java.io.FileReader;


/**
 * Created by babiya on 11.11.2015.
 */
public class BankApplication {
    public static void main(String[] args) throws Exception {
        ActorSystem system = ActorSystem.create("bankSystem");

        final ActorRef processor = system.actorOf(new Props(TransactionProcessorActor.class),"processor");

        BufferedReader br = new BufferedReader(new FileReader("source.csv"));
        String line;
        while ((line = br.readLine()) != null) {
            String[] buff = line.split(";");
            processor.tell(new ChangeAmountMessage(buff[0], Integer.valueOf(buff[1])));
        }
        Thread.sleep(5000);
        processor.tell(new FinalizeResultMessage());
        system.shutdown();
    }
}
